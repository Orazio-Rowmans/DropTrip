<?php
// Insert this to set time limit at all funcion in this class. set time in ms
set_time_limit(0); // 300ms = 5 minutes; 0 = unlimited
require_once 'requireclass.php'; 
class gnjs { 

    function popolationcluster($dbManager) {        

        //place this before any script you want to calculate time
        $start = microtime(true);
        $dbManager->connectDB();        
        $a = 1;
        $c = 1;
        for ($i = 0; $i < 90000; $i++) {          
            if(!(fmod($i, 900) !== 0.00) && $i!=0) {     
                $c++;
                if($c == 26 || $c == 51 || $c == 76 ) {
                $a++;
                }   
            }
            $dbManager->insertRobots($c,$i,$a,$c,1,1,1,1,1,1,1);
        }
        $dbManager->closeDatabase();
        $time_end = microtime(true);
        $execution_time = ($time_end - $start)/60;
        echo $execution_time;
    }

    function popolationarea($dbManager) {
        $idcluster = 0;
        $dbManager->connectDB();
         // insert cluster in area table with ircluster
         for ( $idcluster = 1; $idcluster <= 25; $idcluster++) {
            $m = 1;
            $dbManager->insertArea($m,$idcluster,1);
        }
        for ( $idcluster = 26; $idcluster <= 50; $idcluster++) {
            $m = 2;
            $dbManager->insertArea($m,$idcluster,2);
        }
        for ( $idcluster = 51; $idcluster <= 75; $idcluster++) {
            $m = 3;
            $dbManager->insertArea($m,$idcluster,3);
        }
        for ( $idcluster = 76; $idcluster <= 100; $idcluster++) {
            $m = 4;
            $dbManager->insertArea($m,$idcluster,4);
        }
        $dbManager->closeDatabase();
    }    
}
?>