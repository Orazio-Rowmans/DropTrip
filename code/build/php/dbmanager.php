<?php
class dbManager{

	public $servername = "localhost";
	public $username = "root";
	public $password = "";
	public $dbname = "droptripdb";
	public $sql;
	public $conn;
	public $result;
	function connectDB() {

		// Create connection DB
		$this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

		// Check connection DB
		if ($this->conn->connect_error) {
		    die("Connection failed: " . $this->conn->connect_error);
		}
	}
	
	function checkQuery() {

		if ($this->conn->query($this->sql) === TRUE) {
	    	//echo "New record created successfully";
		} else {
	    	echo "Error: " . $this->sql . "<br>" . $this->conn->error;
		}
	}

	// insert robot's msg in json format in cluster's table 
	function insertRobots($c,$idrobot,$areanum,$idcluster,$s1,$s2,$s3,$s4,$s5,$s6,$s7) { 

		$this->sql = "INSERT INTO cluster$c (idrobot,areanum,idcluster,s1,s2,s3,s4,s5,s6,s7)
					  VALUES ('$idrobot', '$areanum', '$c', '$s1', '$s2', '$s3', '$s4', '$s5', '$s6', '$s7')";

		$this->checkQuery();
	}


	// Create table cluster
	function insertTableCluster($i) {    
		
	$this->sql = "CREATE TABLE cluster$i (
		idrobot int (5) NOT NULL,
		areanum int (1) NOT NULL,
		idcluster int (3) NOT NULL,
		s1 int (1) NOT NULL, 
		s2 int (1) NOT NULL,
		s3 int (1) NOT NULL,
		s4 int (1) NOT NULL,
		s5 int (1) NOT NULL,
		s6 int (1) NOT NULL,
		s7 int (1) NOT NULL,
		submit  TIMESTAMP,
		lstate int (1),
		lsubmit TIMESTAMP,
		PRIMARY KEY (idrobot)	
		)";	
	$this->checkQuery();
	}
	// Create table area
	function insertTableArea($m) {

		$this->sql = "CREATE TABLE area$m (
		idcluster int(3) NOT NULL,
		areanum int(3) NOT NULL		
		)";	
	$this->checkQuery();
	}
	// Insert data in area table
	function insertArea($m,$idcluster,$areanum) {  

		$this->sql = "INSERT INTO area$m (idcluster, areanum)
				VALUES ('$idcluster', '$areanum')";

		$this->checkQuery();
	}

	// CLOSE CONNECTION DB
	function closeDatabase() {  

		$this->conn->close();
	}
	// Select a single cluster table in DB and view this onClick his button
	function read($i) {
		
		$html = "";
		$this->sql = "SELECT * FROM cluster$i";
		if($result = $this->conn->query($this->sql)){
    		if($result->num_rows > 0){
        		$html .= '<table>
        			 <tr>
		                 <th>idrobot</th>
		                 <th>areanum</th>
		                 <th>idcluster</th>
		                 <th>s1</th>
		                 <th>s2</th>
		                 <th>s3</th>
		                 <th>s4</th>
		                 <th>s5</th>
		                 <th>s6</th>
						 <th>s7</th>
						 <th>submit</th>
						 <th>lstate</th>
						 <th>lsubmit</th>
	            	 </tr>';
        			while($row = $result->fetch_array()) {
	            		$html .= '<tr>
		                	<td>'. $row['idrobot'] .'</td>
		                	<td> '. $row['areanum'] .' </td>
		                	<td> '. $row['idcluster'] .' </td>
		                	<td> '. $row['s1'] .' </td>
		                	<td> '. $row['s2'] .' </td>
		                	<td> '. $row['s3'] .' </td>
		                	<td> '. $row['s4'] .' </td>
		                	<td> '. $row['s5'] .' </td>
		                	<td> '. $row['s6'] .' </td>
							<td> '. $row['s7'] .' </td>
							<td> '. $row['submit'] .'</td>
							<td> '. $row['lstate'] .' </td>
							<td> '. $row['lsubmit'] .' </td>';

							
							
	            		$html .= '</tr>';
        		}
					$html .= '</table>';
					echo $html;
					
    		}
		}
	}

	// Update robot in cluster table
	function updaterobot($i,$c) {
		$lstate = 0;
		$this->sql = "SELECT s1,s2,s3,s4,s5,s6,s7,submit FROM cluster$c WHERE idrobot = $i";
		if ($result = $this->conn->query($this->sql)) {
			if ($result->num_rows > 0) {				
				while ($row = $result->fetch_array()) {					
					if ($row['s1']+$row['s2']+$row['s3']+$row['s4']+$row['s5']+$row['s6']+$row['s7'] == 7) {
						$lstate = 1;
					} else {
						$lstate = 0;
					}
					$newstate = random_int(0, 1);
						if ($lstate !== $newstate) {
							$lsubmit = $row['submit'];
							$this->sql = "UPDATE cluster$c SET s1 = '1',
															s2 = '1',
															s3 = '1',
															s4 = '1',
															s5 = '1',
															s6 = '1',
															s7 = '1',
															lsubmit = '$lsubmit',
															lstate ='$lstate'
															WHERE idrobot = $i";
							$this->checkQuery();
							$signal = array ("s1","s2","s3","s4","s5","s6","s7");
							$rand_keys = array_rand($signal, 1);
							$signal[$rand_keys] . "\n";
							$this->sql = "UPDATE cluster$c SET $signal[$rand_keys] = '$newstate' WHERE idrobot = $i";
								if ($this->conn->query($this->sql) === TRUE) {
										//echo "Record updated successfully";
								} else {
									echo "Error updating record: " . $conn->error;
								}
						}
				}
			}
		}
	}
	// Calculate IR cluster and save this in a Array
	function ircluster() {
		$a = 0;
		$b = 0;
		$ircl;
		for ($scrcl =1; $scrcl <= 100; $scrcl++) {
			
			$this->sql = "SELECT s1,s2,s3,s4,s5,s6,s7 FROM cluster$scrcl";
			if($result = $this->conn->query($this->sql)){
				while($row = $result->fetch_array()) {
					if( $row['s1']+$row['s2']+$row['s3']+$row['s4']+$row['s5']+$row['s6']+$row['s7'] == 7 ) {
						$a++;
					} else {
						$b++;
					}
				}
				
			}
			$ircl[] = round(($b*100)/900);
			$a = 0;
			$b = 0;	
		}
	echo json_encode($ircl);	
	}
}
?>