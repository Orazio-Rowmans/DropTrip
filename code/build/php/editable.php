<?php  
require_once 'requireclass.php';

    class editable {

        function clustertable($dbManager) {            
            
            $time_start = microtime(true);
            $dbManager->connectDB();            
            for ($i = 1; $i <= 100; $i++) {
                $dbManager->insertTableCluster($i);
            }
            
            for ($m = 1; $m <= 4; $m++) {
                $dbManager->insertTableArea($m);
            }
            $dbManager->closeDatabase();
            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start)/60;
            echo $execution_time;
        }
    }
?>