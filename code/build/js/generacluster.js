var array;

$(document).ready(function () {	
	for (i=1; i<=100; i++){
		if(i<=25){
			var x = document.createElement('button');
			x.className ="btn";
			var t = document.createTextNode('Cluster '+i);
			x.appendChild(t);
			x.id = i;
			x.setAttribute("onClick","cluster(this.id)");
			document.getElementById("area1").appendChild(x);
		}
		else{
			if(i<=50){
			var x = document.createElement('button');
			x.className ="btn";
			var t = document.createTextNode('Cluster '+i);
			x.appendChild(t);
			x.id = i;
			x.setAttribute("onClick","cluster(this.id)");
			document.getElementById("area2").appendChild(x);
			}
			else{
				if(i<=75){
				var x = document.createElement('button');
				x.className ="btn";
				var t = document.createTextNode('Cluster '+i);
				x.appendChild(t);
				x.id = i;
				x.setAttribute("onClick","cluster(this.id)");
				document.getElementById("area3").appendChild(x);
				}else{
					if(i<=100){
					var x = document.createElement('button');
					x.className ="btn";
					var t = document.createTextNode('Cluster '+i);
					x.appendChild(t);
					x.id = i;
					x.setAttribute("onClick","cluster(this.id)");
					document.getElementById("area4").appendChild(x);
					}
				}
				
			}		
		}			
	}

	$.ajax({
        type: "POST",
        url: 'build/php/requireclass.php',
        data: {
                "functionName": "getir",
                "nCluster": 0
            },
        success: function(response) {          
			var array = JSON.parse(response);
			
			for (i=1; i<=100; i++) {

				document.getElementById(i).innerHTML = "Cluster " + i + " IR : " + array[i-1];
				if (array[i-1]<=50){
					document.getElementById(i).style.background='#4CAF50';
				} else {
					document.getElementById(i).style.background='#f44336';
				}
			}
		}
	});
});

function cluster(clicked_id){
    $.ajax({
        type: "POST",
        url: 'build/php/requireclass.php',
        data: {
                "functionName": "clusterview",
                "nCluster": clicked_id
            },
        success: function(response) {          
            var opened = window.open("cluster");
            opened.document.write(response);
        }
    });
}